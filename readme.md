Beside Module ERP
# Changelog
* 1.2.0 Add custom Curl class to handle HTTP code response, improve log format, fix response parsing.
* 1.0.0 Refactored module logic. Each API interface extends base ErpApiInterface, each API class implements its interface and extends abstract class ErpApi. Add custom serializer to correct encode float values.
* 0.5.0 Change API Authentication method, change configs
* 0.4.0 Add Transfer Order API integration; fix queue messages; change SendQueueRequest file location
* 0.3.0 Add Get Order Details API integration
* 0.2.0 Fix getting promotion rule names
