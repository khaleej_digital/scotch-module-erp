<?php
declare(strict_types=1);

namespace Beside\Erp\Cron;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Beside\Erp\Api\ErpConfigurationInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use DateInterval;
use DateTime as BaseDateTime;
use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Psr\Log\LoggerInterface;

/**
 * Class QueueTableCleaner
 *
 * @package Beside\Erp\Cron
 */
class QueueTableCleaner
{
    /**
     * @var ErpRequestRepositoryInterface
     */
    private ErpRequestRepositoryInterface $erpRequestRepository;

    /**
     * @var ErpConfigurationInterface
     */
    private ErpConfigurationInterface $erpConfiguration;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * QueueTableCleaner constructor.
     *
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     * @param ErpConfigurationInterface $erpConfiguration
     * @param LoggerInterface $logger
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param DateTime $date
     */
    public function __construct(
        ErpRequestRepositoryInterface $erpRequestRepository,
        ErpConfigurationInterface $erpConfiguration,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->erpRequestRepository = $erpRequestRepository;
        $this->erpConfiguration = $erpConfiguration;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->logger = $logger;
    }

    /**
     * Clean up message table
     */
    public function execute()
    {
        try {
            $messages = $this->getExpiredMessages();
            foreach ($messages as $message) {
                $result = $this->erpRequestRepository->delete($message);
                if (!$result) {
                    $this->logger->debug(__('Could not delete entry #' . $message->getId()));
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Get expired messages
     *
     * @return ErpRequestInterface[]
     * @throws Exception
     */
    public function getExpiredMessages()
    {
        $date = $this->getExpireDate();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(ErpRequestInterface::CREATED_AT, $date, 'lt')
            ->create();
        $messages = $this->erpRequestRepository->getList($searchCriteria);
        $messages = $messages->getItems();

        return $messages;
    }

    /**
     * Get expire date - calculate based on config settings
     *
     * @return BaseDateTime
     * @throws Exception
     */
    public function getExpireDate()
    {
        $daysLimit = $this->erpConfiguration->getDaysCount();
        $date = new BaseDateTime('now');
        $interval = new DateInterval(sprintf('P%dD', $daysLimit));
        $date->sub($interval);

        return $date;
    }
}
