<?php
declare(strict_types=1);

namespace Beside\Erp\Cron;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Beside\Erp\Api\ErpConfigurationInterface;
use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Model\Serializer;
use Magento\Framework\DataObject;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;
use Beside\Erp\Model\ErpRequestQueue;

/**
 * Class SendQueueRequest
 * @package Beside\Erp\Cron
 */
class SendQueueRequest
{
    /**
     * Email template Id
     *
     * @var string
     */
    const EMAIL_TEMPLATE_ID = 'beside_fail_request_api_email_template';

    /**
     * @var int
     */
    private $attemptsCount;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var ErpRequestRepositoryInterface
     */
    private ErpRequestRepositoryInterface $erpRequestRepository;

    /**
     * @var ErpConfigurationInterface
     */
    private ErpConfigurationInterface $erpConfiguration;

    /**
     * @var TransportBuilder
     */
    private TransportBuilder $transportBuilder;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ErpRequestQueue
     */
    private ErpRequestQueue $erpRequestQueue;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * @var array
     */
    private $entityIdToEntityTypeMapper = [
        'rma_id' => \Beside\Erp\Api\CreateReturnApiInterface::API_TYPE,
        'order_id' => \Beside\Erp\Api\OrderCreateApiInterface::API_TYPE
    ];

    /**
     * SendQueueRequest constructor.
     *
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     * @param ErpConfigurationInterface $erpConfiguration
     * @param TransportBuilder $transportBuilder
     * @param LoggerInterface $logger
     * @param ErpRequestQueue $erpRequestQueue
     * @param Serializer $serializer
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ErpRequestRepositoryInterface $erpRequestRepository,
        ErpConfigurationInterface $erpConfiguration,
        TransportBuilder $transportBuilder,
        LoggerInterface $logger,
        ErpRequestQueue $erpRequestQueue,
        Serializer $serializer,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->erpRequestRepository = $erpRequestRepository;
        $this->erpConfiguration = $erpConfiguration;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $logger;
        $this->erpRequestQueue = $erpRequestQueue;
        $this->serializer = $serializer;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    /**
     * Send request from queue
     */
    public function execute()
    {
        $this->attemptsCount = $this->erpConfiguration->getAttemptsCount() ?? 0;
        $requestList = $this->getFailRequestList($this->attemptsCount);
        $dependentRequests = [];

        foreach ($requestList as $request) {
            if ($request->getMessageToSend()) {
                if (\Beside\Erp\Api\CreateTransferOrderApiInterface::API_TYPE == $request->getApiType()) {
                    $dependentRequests['order_id'][] = $request;
                    continue;
                }

                if (\Beside\Erp\Api\UpdateReturnApiInterface::API_TYPE == $request->getApiType()) {
                    $dependentRequests['rma_id'][] = $request;
                    continue;
                }

                $this->erpRequestQueue->update($request);

                if ($request->getStatus() === ErpRequestInterface::STATUS_FAIL
                    && $this->attemptsCount >= $request->getNumTries()
                ) {
                    $this->sendEmailFailRequestApi($request);
                }
            }
        }

        if (!empty($dependentRequests)) {
            $this->validateAndSend($dependentRequests);
        }
    }

    /**
     *  Checks if the request has corresponding successful requests and sends it.
     *  E.g. transger_order rely on create_order, update_return rely on create_return
     *
     * @param array $dependentRequests
     */
    private function validateAndSend(array $dependentRequests)
    {
        foreach ($dependentRequests as $entityIdKey => $requests) {
            $sortedRequests = [];

            foreach ($requests as $request) {
                $entityId = $this->getDataFromMessageBody($request, $entityIdKey);

                if ($entityId) {
                    //Such implementation not allows to send multiple TO for one order.
                    //Rest of TO's will be processed on next cron run.
                    $sortedRequests[$entityId] = $request;
                }
            }

            if (count($sortedRequests)) {
                $relatedRequests = $this->getSuccessRequestList(array_keys($sortedRequests), $entityIdKey);

                if (!empty($relatedRequests)) {
                    foreach ($relatedRequests as $relatedRequest) {
                        $entityId = $this->getDataFromMessageBody($relatedRequest, $entityIdKey);

                        if (isset($sortedRequests[$entityId])) {
                            $this->erpRequestQueue->update($sortedRequests[$entityId]);
                        }

                        if ($sortedRequests[$entityId]->getStatus() === ErpRequestInterface::STATUS_FAIL
                            && $this->attemptsCount >= $sortedRequests[$entityId]->getNumTries()
                        ) {
                            $this->sendEmailFailRequestApi($sortedRequests[$entityId]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param ErpRequestInterface $request
     * @param string $dataKey
     * @return string|null
     */
    private function getDataFromMessageBody(ErpRequestInterface $request, string $dataKey)
    {
        $messageBody = $request->getMessageToSend();
        $requestData = $this->serializer->unserialize($messageBody);

        return $requestData[$dataKey] ?? null;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSuccessRequestList(array $ids, string $entityIdKey): array
    {
        $likeFilterGroup = $this->filterGroupBuilder->create();
        $likeFilters = [];

        foreach ($ids as $id) {
            $filter = $this->filterBuilder->create();
            $searchValue = strtr('%":entity_id_key":":entity_id_value"%', [':entity_id_key' => $entityIdKey, ':entity_id_value' => $id]);

            $filter->setField(ErpRequestInterface::MESSAGE_TO_SEND)
                ->setValue($searchValue)
                ->setConditionType('like');

            $likeFilters[] = $filter;
        }
        $likeFilterGroup->setFilters($likeFilters);

        $statusFilter = $this->filterBuilder->create();
        $statusFilter->setField(ErpRequestInterface::STATUS)
            ->setValue(ErpRequestInterface::STATUS_SUCCESS)
            ->setConditionType('eq');

        $statusFilterGroup = $this->filterGroupBuilder->create();
        $statusFilterGroup->setFilters([$statusFilter]);

        $entityTypeFilter = $this->filterBuilder->create();
        $entityTypeFilter->setField(ErpRequestInterface::API_TYPE)
            ->setValue($this->entityIdToEntityTypeMapper[$entityIdKey])
            ->setConditionType('eq');

        $entityTypeFilterGroup = $this->filterGroupBuilder->create();
        $entityTypeFilterGroup->setFilters([$entityTypeFilter]);

        $searchCriteria = $this->searchCriteriaBuilder;

        $searchCriteria->setFilterGroups([
            $likeFilterGroup,
            $statusFilterGroup,
            $entityTypeFilterGroup
        ]);

        return $this->erpRequestRepository
            ->getList($searchCriteria->create())
            ->getItems();
    }

    /**
     * Get fail request list
     *
     * @param int $attemptsCount
     * @return array
     */
    public function getFailRequestList(int $attemptsCount): array
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(ErpRequestInterface::STATUS, ErpRequestInterface::STATUS_FAIL, 'eq')
            ->addFilter(ErpRequestInterface::NUM_TRIES, $attemptsCount, 'lt');

        return $this->erpRequestRepository->getList($searchCriteria->create())->getItems();
    }

    /**
     * Send email fail request API
     *
     * @param $request
     * @return void
     */
    private function sendEmailFailRequestApi($request)
    {
        if ($this->erpConfiguration->getEmailNotificationActive()) {
            $emailTempVariables = ['msg'=> $request->getId()];
            $emailTo = $this->erpConfiguration->getAlertEmail();
            $emailFrom = ['email' => $this->erpConfiguration->getSenderEmail(), 'name' => $this->erpConfiguration->getSenderEmail()];

            try {
                $postObject = new DataObject();
                $postObject->setData($emailTempVariables);
                $error = false;

                if (!\Zend_Validate::is(trim($emailTo), 'EmailAddress')) {
                    $error = true;
                }

                if ($error) {
                    throw new \Exception();
                }

                $transport = $this->transportBuilder
                    ->setTemplateIdentifier(self::EMAIL_TEMPLATE_ID)
                    ->setTemplateOptions(
                        [
                            'area' => FrontNameResolver::AREA_CODE,
                            'store' => Store::DEFAULT_STORE_ID,
                        ]
                    )
                    ->setTemplateVars(['data' => $postObject])
                    ->addTo($emailTo)
                    ->setFrom($emailFrom)
                    ->getTransport();
                $transport->sendMessage();

            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }
}
