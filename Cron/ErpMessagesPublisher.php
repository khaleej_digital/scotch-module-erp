<?php
declare(strict_types=1);

namespace Beside\Erp\Cron;

use Beside\Erp\Api\OrderCreateApiInterface;
use Beside\Erp\Api\CreateTransferOrderApiInterface;
use Beside\Erp\Model\OrderDataBuilder;
use Beside\Sourcing\Api\AvailabilityRequesterInterface;
use Beside\Erp\Api\ErpConfigurationInterface;
use Beside\Erp\Model\ResourceModel\BulkProcessor;
use Exception;
use http\Params;
use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\DataObject;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * Class ErpMessagesPublisher
 *
 * @package Beside\Erp\Cron
 */
class ErpMessagesPublisher
{
    const EMAIL_TEMPLATE_ID = 'beside_erp_could_not_process_order_email_template';

    /**
     * @var OrderDataBuilder
     */
    private OrderDataBuilder $orderDataBuilder;

    /**
     * @var OrderCreateApiInterface
     */
    private OrderCreateApiInterface $orderCreateApi;

    /**
     * @var AvailabilityRequesterInterface
     */
    private AvailabilityRequesterInterface $availabilityRequester;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ErpConfigurationInterface
     */
    private $erpConfiguration;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var BulkProcessor
     */
    private $bulkProcessor;

    /**
     * ErpMessagesPublisher constructor.
     * @param OrderDataBuilder $orderDataBuilder
     * @param AvailabilityRequesterInterface $availabilityRequester
     * @param LoggerInterface $logger
     * @param OrderCreateApiInterface $orderCreateApi
     * @param ErpConfigurationInterface $erpConfiguration
     * @param TransportBuilder $transportBuilder
     * @param BulkProcessor $bulkProcessor
     */
    public function __construct(
        OrderDataBuilder $orderDataBuilder,
        AvailabilityRequesterInterface $availabilityRequester,
        LoggerInterface $logger,
        OrderCreateApiInterface $orderCreateApi,
        ErpConfigurationInterface $erpConfiguration,
        TransportBuilder $transportBuilder,
        BulkProcessor $bulkProcessor
    ) {
        $this->orderDataBuilder = $orderDataBuilder;
        $this->orderCreateApi = $orderCreateApi;
        $this->availabilityRequester = $availabilityRequester;
        $this->logger = $logger;
        $this->erpConfiguration = $erpConfiguration;
        $this->transportBuilder = $transportBuilder;
        $this->bulkProcessor = $bulkProcessor;
    }

    /**
     * Cron execute method
     */
    public function execute(): void
    {
        $failedOrders = [];
        $collection = $this->orderDataBuilder->getErpUnprocessedOrders();

        /** @var OrderInterface $order */
        foreach ($collection->getItems() as $order) {
            try {
                $orderId = $order->getEntityId();
                $this->availabilityRequester->cleanCurrentItemsArray();
                $this->availabilityRequester->getItemsStock($order);
                $coordinatesAvailable = $order->getLatitude() && $order->getLongitude();

                if ($coordinatesAvailable) {
                    $consolidatingStore = $this->availabilityRequester->resolveConsolidatingStore($order);
                    $order->getBillingAddress()->setPickupLocationId($consolidatingStore);
                } elseif (!$coordinatesAvailable && !$order->getBillingAddress()->getPickupLocationId()) {
                    throw new Exception('Pick-up location ID or Customer coordinates missing.');
                }

                $message = $this->orderCreateApi->prepareData($order);
                $transferOrders = $this->availabilityRequester->getTransferOrderMessages($order);

                $processedOrder = [
                    'order' => $order,
                    'create_order_message' => $message,
                    'transfer_orders' => $transferOrders
                ];
                $this->updateProcessedOrders($processedOrder);

            } catch (Exception $exception) {
                $failedOrders[$orderId] = $exception->getMessage();
                $this->logger->critical($exception->getMessage());
                $this->updateFailedOrders($order);
                continue;
            }
        }

        if (!empty($failedOrders)) {
            $this->sendEmailFailOrder($failedOrders);
        }
    }

    /**
     * @param array $failedOrders
     */
    private function sendEmailFailOrder(array $failedOrders)
    {
        if ($this->erpConfiguration->getEmailNotificationActive()) {
            $lineTemplate = '<table><tr><td>Order ID: %1</td><tr><tr><td>Reason: %2</td><tr></table>';
            $content = '';

            foreach ($failedOrders as $orderId => $message) {
                $content .= __($lineTemplate, $orderId, $message);
            }

            $emailTempVariables = ['email_content' => $content];
            $emailTo = $this->erpConfiguration->getAlertEmail();
            $emailFrom = [
                'email' => $this->erpConfiguration->getSenderEmail(),
                'name' => $this->erpConfiguration->getSenderEmail()
            ];

            try {
                $error = false;

                if (!\Zend_Validate::is(trim($emailTo), 'EmailAddress')) {
                    $error = true;
                }

                if ($error) {
                    throw new \Exception('Could not send email for failed Order. Order ID ' . $orderId);
                }

                $transport = $this->transportBuilder
                    ->setTemplateIdentifier(self::EMAIL_TEMPLATE_ID)
                    ->setTemplateOptions(
                        [
                            'area' => FrontNameResolver::AREA_CODE,
                            'store' => Store::DEFAULT_STORE_ID,
                        ]
                    )
                    ->setTemplateVars($emailTempVariables)
                    ->addTo($emailTo)
                    ->setFrom($emailFrom)
                    ->getTransport();
                $transport->sendMessage();

            } catch (\Exception $e) {
                $this->logger->error(self::class . ': ' .$e->getMessage());
            }
        }
    }

    /**
     * @param array $array
     */
    private function updateProcessedOrders(array $processedOrdersData)
    {
        /** @var OrderInterface $order */
        $order = $processedOrdersData['order'];
        $preparedOrderData[] = [
            'entity_id' => $order->getEntityId(),
            'is_processed' => 1,
            'num_tries' => $order->getNumTries() + 1
        ];

        $this->bulkProcessor->bulkUpdate('sales_order', $preparedOrderData);

        $createOrderMessage = $processedOrdersData['create_order_message'];
        $preparedCreateOrderData = $this->prepareMessages(
            [$createOrderMessage],
            OrderCreateApiInterface::API_TYPE,
            $order->getStoreId()
        );

        $transferOrderMessages = array_filter($processedOrdersData['transfer_orders']);
        $preparedTransferOrderData = $this->prepareMessages(
            $transferOrderMessages,
            CreateTransferOrderApiInterface::API_TYPE,
            $order->getStoreId()
        );

        $messages = array_merge($preparedCreateOrderData, $preparedTransferOrderData);
        $this->bulkProcessor->bulkInsert('beside_erp_queue', $messages);
    }

    /**
     * @param OrderInterface $order
     */
    private function updateFailedOrders(OrderInterface $order)
    {
        $data = [
            'entity_id' => $order->getEntityId(),
            'num_tries' => $order->getNumTries() + 1
        ];

        $this->bulkProcessor->bulkUpdate('sales_order', $data);
    }

    /**
     * @param array $messages
     * @param string $apiType
     * @param int|null $storeId
     * @return array
     */
    private function prepareMessages(array $messages, string $apiType, $storeId = null)
    {
        $preparedData = [];
        foreach ($messages as $messageData) {
            $preparedData[] = [
                'api_type' => $apiType,
                'message_to_send' => $messageData,
                'store_id' => $storeId,
                'status' => 'fail'
            ];
        }

        return $preparedData;
    }
}
