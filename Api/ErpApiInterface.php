<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Exception;

/**
 * Interface ErpApiInterface
 *
 * @package Beside\Erp\Api
 */
interface ErpApiInterface
{
    /**
     * Send request to API based on API type
     *
     * @param string $data
     * @param string|null $scopeType
     * @param string|null $storeCode
     * @return array
     */
    public function sendRequest(string $data, $scopeType = null, $storeCode = null): array;

    /**
     * Save message to queue
     *
     * @param string $message
     * @param int $storeId
     * @return ErpRequestInterface|null
     */
    public function saveToQueue(string $message, int $storeId): ?ErpRequestInterface;
}
