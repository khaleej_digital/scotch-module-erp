<?php
declare(strict_types=1);

namespace Beside\Erp\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ErpRequestSearchResultInterface
 *
 * @package Beside\Erp\Api\Data
 */
interface ErpRequestSearchResultInterface extends SearchResultsInterface
{
    /**
     * Get request list
     *
     * @return ErpRequestInterface[]
     */
    public function getItems(): array;

    /**
     * @param ErpRequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items): ErpRequestSearchResultInterface;
}
