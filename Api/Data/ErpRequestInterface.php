<?php
declare(strict_types=1);

namespace Beside\Erp\Api\Data;

/**
 * Interface ErpRequestInterface
 *
 * @package Beside\Erp\Api\Data
 */
interface ErpRequestInterface
{
    /**
     * Status fail
     */
    const STATUS_FAIL = 'fail';

    /**
     * Status success
     */
    const STATUS_SUCCESS = 'success';

    /**
     * Flag if order is processed (ERP messages are build)
     */
    const ORDER_IS_PROCESSED_FLAG = 'is_processed';

    /**
     * Constants defined for keys of data array
     */
    const ID = 'id';
    const API_TYPE = 'api_type';
    const MESSAGE_TO_SEND = 'message_to_send';
    const NUM_TRIES = 'num_tries';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const API_RESPONSE = 'api_response';
    const STORE_ID = 'store_id';

    /**
     * Request id
     *
     * @return mixed
     */
    public function getId();

    /**
     * Set request id
     *
     * @param int $id
     * @return $this
     */
    public function setId(int $id): ErpRequestInterface;

    /**
     * Get type of API
     *
     * @return string|null
     */
    public function getApiType(): ?string;

    /**
     * Set type of API
     *
     * @param string $apiType
     * @return $this
     */
    public function setApiType(string $apiType): ErpRequestInterface;

    /**
     * Get message to send
     *
     * @return string|null
     */
    public function getMessageToSend(): ?string;

    /**
     * Set message to send
     *
     * @param string $message
     * @return $this
     */
    public function setMessageToSend(string $message): ErpRequestInterface;

    /**
     * Get num of tries
     *
     * @return int|null
     */
    public function getNumTries(): ?int;

    /**
     * Set num of tries
     *
     * @param int $numTries
     * @return $this
     */
    public function setNumTries(int $numTries): ErpRequestInterface;

    /**
     * Get status of request
     *
     * @return string|null
     */
    public function getStatus(): ?string;

    /**
     * Set status of request
     *
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): ErpRequestInterface;

    /**
     * Retrieve creation date and time
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Retrieve updated date and time
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * Set api response
     *
     * @param string $apiResponse
     * @return ErpRequestInterface
     */
    public function setApiResponse(string $apiResponse): ErpRequestInterface;

    /**
     * Get api response
     *
     * @return string|null
     */
    public function getApiResponse(): ?string;

    /**
     * @param int $apiResponse
     * @return ErpRequestInterface
     */
    public function setStoreId(int $storeId): ErpRequestInterface;

    /**
     * @return int|null
     */
    public function getStoreId(): ?int;
}
