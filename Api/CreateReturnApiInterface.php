<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use InvalidArgumentException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Rma\Api\Data\RmaInterface;

/**
 * Interface CreateReturnApiInterface
 *
 * @package Beside\Erp\Api
 */
interface CreateReturnApiInterface extends ErpApiInterface
{
    /**
     * Create Return API
     */
    public const API_TYPE = 'create_return';

    /**
     * Prepare data for Create Return API
     *
     * @param RmaInterface $rmaEntity
     *
     * @return string
     * @throws InvalidArgumentException
     * @throws NoSuchEntityException
     */
    public function prepareData(RmaInterface $rmaEntity): string;
}
