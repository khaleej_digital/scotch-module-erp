<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use InvalidArgumentException;

/**
 * Interface StockAggregateApiInterface
 *
 * @package Beside\Erp\Api
 */
interface StockAggregateApiInterface extends ErpApiInterface
{
    /**
     * Stock Check API type
     */
    public const API_TYPE = 'stock_aggregate';

    /**
     * Prepare data, returns json-encoded string
     *
     * @param string $sku
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(string $sku): string;
}
