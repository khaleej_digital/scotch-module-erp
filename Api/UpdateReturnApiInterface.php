<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use InvalidArgumentException;
use Magento\Rma\Api\Data\RmaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface UpdateReturnApiInterface
 *
 * @package Beside\Erp\Api
 */
interface UpdateReturnApiInterface extends ErpApiInterface
{
    /**
     * Create Return API
     */
    public const API_TYPE = 'update_return';

    /**
     * Prepare data from RMA for Return API
     *
     * @param RmaInterface $rmaEntity
     *
     * @return string
     * @throws InvalidArgumentException
     * @throws NoSuchEntityException
     */
    public function prepareData(RmaInterface $rmaEntity): string;
}
