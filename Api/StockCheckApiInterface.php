<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

/**
 * Interface StockCheckApiInterface
 *
 * @package Beside\Erp\Api
 */
interface StockCheckApiInterface extends ErpApiInterface
{
    /**
     * Stock Check API type
     */
    public const API_TYPE = 'stock_check';

    /**
     * Prepare data, return json-encoded string
     *
     * @param string $sku
     * @param string|null $sourceId
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function prepareData(string $sku, $sourceId = null): string;
}
