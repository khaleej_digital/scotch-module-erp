<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use Magento\Sales\Api\Data\OrderInterface;

/**
 * Interface CreateTransferOrderApiInterface
 *
 * @package Beside\Erp\Api
 */
interface CreateTransferOrderApiInterface extends ErpApiInterface
{
    /**
     * Transfer Order API type
     */
    public const API_TYPE = 'transfer_order';
}
