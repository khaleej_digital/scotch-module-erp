<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Beside\Erp\Api\Data\ErpRequestSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ErpRequestRepositoryInterface
 *
 * @package Beside\Erp\Api
 */
interface ErpRequestRepositoryInterface
{
    /**
     * Get info about request by request id
     *
     * @param int $id
     * @return ErpRequestInterface
     */
    public function get(int $id): ErpRequestInterface;

    /**
     * Get request list
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ErpRequestSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Create request
     *
     * @param ErpRequestInterface $request
     * @return ErpRequestInterface
     */
    public function save(ErpRequestInterface $request): ErpRequestInterface;

    /**
     * Delete request
     *
     * @param ErpRequestInterface $request
     * @return bool Will returned True if deleted
     */
    public function delete(ErpRequestInterface $request): bool;

    /**
     * Delete request by id
     *
     * @param int $id
     * @return bool Will returned True if deleted
     */
    public function deleteById(int $id): bool;
}
