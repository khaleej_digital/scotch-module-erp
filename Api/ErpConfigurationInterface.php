<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

/**
 * Interface ErpConfigurationInterface
 *
 * @package Beside\Erp\Api
 */
interface ErpConfigurationInterface
{
    /**
     * XML path to cron schedule expression for message sending
     */
    public const XML_PATH_CRON_SCHEDULE_SEND = 'beside_erp/general/cron_schedule_send';

    /**
     * XML path to cron schedule expression for messages deletion
     */
    public const XML_PATH_CRON_SCHEDULE_DELETE = 'beside_erp/general/cron_schedule_delete';

    /**
     * XML path to timeout config for waiting for connection with the API
     */
    public const XML_PATH_CONNECTION_TIMEOUT = 'beside_erp/general/connection_timeout';

    /**
     * XML path to timeout config for execution timeout
     */
    public const XML_PATH_EXECUTION_TIMEOUT = 'beside_erp/general/execution_timeout';

    /**
     * XML path to retry attempts count
     */
    public const XML_PATH_ATTEMPTS_COUNT = 'beside_erp/general/attempts_count';

    /**
     * XML path to alert email recipient address
     */
    public const XML_PATH_ALERT_EMAIL = 'beside_erp/general/alert_email';

    /**
     * XML path to sender email
     */
    public const XML_PATH_SENDER_EMAIL = 'beside_erp/general/sender_email';

    /**
     * XML path to days count for queue messages
     */
    public const XML_PATH_DAYS_COUNT = 'beside_erp/general/days_count';

    /**
     * XML path to Carrier code setting
     */
    public const XML_PATH_CARRIER_CODE = 'beside_erp/create_order/carrier_code';

    /**
     * XML path to logger setting
     */
    public const XML_PATH_LOGGER_ACTIVE = 'beside_erp/general/logger_active';

    /**
     * XML path to authentication username setting
     */
    public const XML_PATH_AUTH_USERNAME = 'beside_erp/general/username';

    /**
     * XML path to authentication password setting
     */
    public const XML_PATH_AUTH_PASSWORD = 'beside_erp/general/password';

    /**
     * XML path to authentication password setting
     */
    public const XML_PATH_EMAIL_NOTIFICATION_ACTIVE = 'beside_erp/general/email_notification_active';

    /**
     * If Email Notification is active.
     *
     * @return bool
     */
    public function getEmailNotificationActive(): bool;

    /**
     * Get cron schedule expression for message sending
     *
     * @return string|null
     */
    public function getCronSendingScheduleValue(): ?string;

    /**
     * Get cron schedule expression for message deletion
     *
     * @return string|null
     */
    public function getCronDeletionScheduleValue(): ?string;

    /**
     * @return int|null
     */
    public function getConnectionTimeout(): ?int;

    /**
     * @return int|null
     */
    public function getExecutionTimeout(): ?int;

    /**
     * @return int|null
     */
    public function getAttemptsCount(): ?int;

    /**
     * Get aler email
     *
     * @return string|null
     */
    public function getAlertEmail(): ?string;

    /**
     * Get days count for queue messages
     *
     * @return int|null
     */
    public function getDaysCount(): ?int;

    /**
     * Get Sender Email
     *
     * @return string|null
     */
    public function getSenderEmail(): ?string;

    /**
     * @param string $scopeType
     * @param string $scopeCode
     * @return string|null
     */
    public function getAuthUsername(?string $scopeType, ?string $scopeCode): ?string;

    /**
     * @param string $scopeType
     * @param string $scopeCode
     * @return string|null
     */
    public function getAuthPassword(?string $scopeType, ?string $scopeCode): ?string;
}
