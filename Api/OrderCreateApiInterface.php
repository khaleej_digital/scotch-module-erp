<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Exception;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Interface OrderCreateApiInterface
 *
 * @package Beside\Erp\Api
 */
interface OrderCreateApiInterface extends ErpApiInterface
{
    /**
     * Stock Check API type
     */
    public const API_TYPE = 'create_order';

    /**
     * Prepare data for request
     *
     * @param OrderInterface $order
     *
     * @return string
     */
    public function prepareData(OrderInterface $order): string;
}
