<?php
declare(strict_types=1);

namespace Beside\Erp\Api;

use InvalidArgumentException;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Interface GetOrderDetailsApiInterface
 *
 * @package Beside\Erp\Api
 */
interface GetOrderDetailsApiInterface extends ErpApiInterface
{
    /**
     * Get Order Details API type
     */
    public const API_TYPE = 'get_order_details';

    /**
     * Prepare data from order to API call, return json-encoded string
     *
     * @param OrderInterface $order
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(OrderInterface $order): string;
}
