<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\ErpConfigurationInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Api\OrderCreateApiInterface;
use InvalidArgumentException;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Psr\Log\LoggerInterface;
use Beside\Erp\Model\HTTP\Client\BesideCurlFactory as CurlFactory;
use Beside\Erp\Model\ErpRequestFactory as ErpRequestFactory;

/**
 * Class OrderCreateApi
 *
 * @package Beside\Erp\Model
 */
class OrderCreateApi extends ErpApi implements OrderCreateApiInterface
{
    /**
     * Api type
     *
     * @var string
     */
    public $apiType = self::API_TYPE;

    /**
     * @var OrderDataBuilder
     */
    private OrderDataBuilder $orderDataBuilder;

    /**
     * OrderCreateApi constructor.
     *
     * @param OrderDataBuilder $orderDataBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param ErpConfigurationInterface $erpConfiguration
     * @param CurlFactory $curlFactory
     * @param Serializer $serializer
     * @param LoggerInterface $logger
     * @param ErpRequestFactory $erpRequestFactory
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     */
    public function __construct(
        OrderDataBuilder $orderDataBuilder,
        ScopeConfigInterface $scopeConfig,
        ErpConfigurationInterface $erpConfiguration,
        CurlFactory $curlFactory,
        Serializer $serializer,
        LoggerInterface $logger,
        ErpRequestFactory $erpRequestFactory,
        ErpRequestRepositoryInterface $erpRequestRepository
    ) {
        parent::__construct(
            $scopeConfig,
            $erpConfiguration,
            $curlFactory,
            $serializer,
            $logger,
            $erpRequestFactory,
            $erpRequestRepository
        );
        $this->orderDataBuilder = $orderDataBuilder;
    }

    /**
     * Prepare data from order to API call
     *
     * @param OrderInterface $order
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(OrderInterface $order): string
    {
        $orderData = $this->orderDataBuilder->getOrderDataArray($order);
        $message = $this->serializer->serialize($orderData);

        return $message;
    }
}
