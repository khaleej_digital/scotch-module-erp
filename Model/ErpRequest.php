<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class ErpRequest
 *
 * @package Beside\Erp\Model
 */
class ErpRequest extends AbstractModel implements ErpRequestInterface, IdentityInterface
{
    /**
     * Request cache tag
     */
    const CACHE_TAG = 'beside_erp_message';

    /**
     * Model for object initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\ErpRequest::class);
    }

    /**
     * Get Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(ErpRequestInterface::ID);
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return $this|ErpRequest
     */
    public function setId($id): ErpRequest
    {
        $this->setData(ErpRequestInterface::ID, $id);
        return $this;
    }

    /**
     * Get type of API
     *
     * @return string|null
     */
    public function getApiType(): ?string
    {
        return $this->getData(ErpRequestInterface::API_TYPE);
    }

    /**
     * Set type of API
     *
     * @param string $apiType
     *
     * @return $this|ErpRequest
     */
    public function setApiType(string $apiType): ErpRequest
    {
        $this->setData(ErpRequestInterface::API_TYPE, $apiType);
        return $this;
    }

    /**
     * Get message to send
     *
     * @return string|null
     */
    public function getMessageToSend(): ?string
    {
        return $this->getData(ErpRequestInterface::MESSAGE_TO_SEND);
    }

    /**
     * Set message to send
     *
     * @param string $message
     *
     * @return $this|ErpRequest
     */
    public function setMessageToSend(string $message): ErpRequest
    {
        $this->setData(ErpRequestInterface::MESSAGE_TO_SEND, $message);
        return $this;
    }

    /**
     * Get num tries
     *
     * @return int|null
     */
    public function getNumTries(): ?int
    {
        return (int) $this->getData(ErpRequestInterface::NUM_TRIES);
    }

    /**
     * Set num tries
     *
     * @param int $numTries
     *
     * @return $this|ErpRequest
     */
    public function setNumTries(int $numTries): ErpRequest
    {
        $this->setData(ErpRequestInterface::NUM_TRIES, $numTries);
        return $this;
    }

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->getData(ErpRequestInterface::STATUS);
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return $this|ErpRequest
     */
    public function setStatus(string $status): ErpRequest
    {
        return $this->setData(ErpRequestInterface::STATUS, $status);
        return $this;
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Retrieve creation date and time
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(ErpRequestInterface::CREATED_AT);
    }

    /**
     * Get updated date and time
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->getData(ErpRequestInterface::UPDATED_AT);
    }

    /**
     * Get api response
     *
     * @return string|null
     */
    public function getApiResponse(): ?string
    {
        return $this->getData(ErpRequestInterface::API_RESPONSE);
    }

    /**
     * Set api response
     *
     * @param string $apiResponse
     * @return ErpRequest
     */
    public function setApiResponse(string $apiResponse): ErpRequest
    {
        $this->setData(ErpRequestInterface::API_RESPONSE, $apiResponse);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStoreId(): ?int
    {
        $value = $this->getData(ErpRequestInterface::STORE_ID);

        return (int) $value ?: null;
    }

    /**
     * @param int $storeId
     * @return ErpRequest
     */
    public function setStoreId(int $storeId): ErpRequest
    {
        $this->setData(ErpRequestInterface::STORE_ID, $storeId);
        return $this;
    }
}
