<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\ErpConfigurationInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Api\UpdateReturnApiInterface;
use InvalidArgumentException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Rma\Api\Data\RmaInterface;
use Psr\Log\LoggerInterface;
use Beside\Erp\Model\HTTP\Client\BesideCurlFactory as CurlFactory;
use Beside\Erp\Model\ErpRequestFactory as ErpRequestFactory;
use Beside\Erp\Model\Serializer;

/**
 * Class UpdateReturnApi
 *
 * @package Beside\Erp\Model
 */
class UpdateReturnApi extends ErpApi implements UpdateReturnApiInterface
{
    const RETURN_VALUE_KEY = 'return_value';
    const RETURN_VALUE = 'Update Return';
    /**
     * @var RmaDataFetcher
     */
    private RmaDataFetcher $rmaDataFetcher;

    /**
     * UpdateReturnApi constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErpConfigurationInterface $erpConfiguration
     * @param CurlFactory $curlFactory
     * @param Serializer $serializer
     * @param LoggerInterface $logger
     * @param ErpRequestFactory $erpRequestFactory
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     * @param RmaDataFetcher $rmaDataFetcher
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErpConfigurationInterface $erpConfiguration,
        CurlFactory $curlFactory,
        Serializer $serializer,
        LoggerInterface $logger,
        ErpRequestFactory $erpRequestFactory,
        ErpRequestRepositoryInterface $erpRequestRepository,
        RmaDataFetcher $rmaDataFetcher
    ) {
        parent::__construct(
            $scopeConfig,
            $erpConfiguration,
            $curlFactory,
            $serializer,
            $logger,
            $erpRequestFactory,
            $erpRequestRepository
        );
        $this->rmaDataFetcher = $rmaDataFetcher;
    }

    /**
     * Api type
     * @var string
     */
    public $apiType = self::API_TYPE;


    /**
     * Prepare data from RMA for Return API
     *
     * @param RmaInterface $rmaEntity
     *
     * @return string
     * @throws InvalidArgumentException
     * @throws NoSuchEntityException
     */
    public function prepareData(RmaInterface $rmaEntity): string
    {
        $data = $this->rmaDataFetcher->getDetails($rmaEntity, self::RETURN_VALUE);
        $data[self::RETURN_VALUE_KEY] = self::RETURN_VALUE;
        $data['created_at'] = $rmaEntity->getDateRequested();

        $message = $this->serializer->serialize($data);

        return $message;
    }
}
