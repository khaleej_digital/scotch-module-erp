<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\ErpConfigurationInterface;
use Exception;
use InvalidArgumentException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ClientInterface;
use Psr\Log\LoggerInterface;
use Beside\Erp\Model\HTTP\Client\BesideCurlFactory as CurlFactory;
use Beside\Erp\Model\ErpRequestFactory as ErpRequestFactory;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Api\Data\ErpRequestInterface;

/**
 * Class ErpApi
 *
 * @package Beside\Erp\Model
 */
abstract class ErpApi
{
    /**
     * Api type
     * @var string
     */
    public $apiType;

    /**
     * @var ErpConfigurationInterface
     */
    private ErpConfigurationInterface $erpConfiguration;

    /**
     * @var CurlFactory
     */
    private CurlFactory $curlFactory;

    /**
     * @var Serializer
     */
    public Serializer $serializer;

    /**
     * @var LoggerInterface
     */
    public LoggerInterface $logger;

    /**
     * @var ScopeConfigInterface
     */
    public ScopeConfigInterface $scopeConfig;

    /**
     * @var ErpRequestFactory
     */
    private ErpRequestFactory $erpRequestFactory;

    /**
     * @var ErpRequestRepositoryInterface
     */
    private ErpRequestRepositoryInterface $erpRequestRepository;

    /**
     * ErpApi constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErpConfigurationInterface $erpConfiguration
     * @param CurlFactory $curlFactory
     * @param Serializer $serializer
     * @param LoggerInterface $logger
     * @param ErpRequestFactory $erpRequestFactory
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErpConfigurationInterface $erpConfiguration,
        CurlFactory $curlFactory,
        Serializer $serializer,
        LoggerInterface $logger,
        ErpRequestFactory $erpRequestFactory,
        ErpRequestRepositoryInterface $erpRequestRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->erpConfiguration = $erpConfiguration;
        $this->curlFactory = $curlFactory;
        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->erpRequestFactory = $erpRequestFactory;
        $this->erpRequestRepository = $erpRequestRepository;
    }

    /**
     * Get API type
     *
     * @return string|null
     *
     * @throws Exception
     */
    public function getApiType(): ?string
    {
        if ($this->apiType === null) {
            throw new Exception('API type is not defined');
        } else {
            return $this->apiType;
        }
    }

    /**
     * Send request to API or throw an Exception
     *
     * @param string $data
     *
     * @return array|string[]
     * @throws Exception
     */
    public function sendRequest(string $data, $scopeType = null, $scopeCode = null): array
    {
        if ($this->getApiType()){
            return $this->sendRequestToApi($data, $scopeType, $scopeCode);
        }
    }

    /**
     * Send request to API based on api_type
     *
     * @param string $data
     * @param string|null $scopeType
     * @param string|null $scopeCode
     * @return array
     */
    private function sendRequestToApi(string $data, $scopeType = null, $scopeCode = null): array
    {
        if (!$this->isApiEnabled($scopeType, $scopeCode)) {
            $result = ['response' => '', 'errors' => 'API disabled'];
        } else {
            /** @var \Beside\Erp\Model\HTTP\Client\BesideCurl $curl */
            $curl = $this->curlFactory->create() ;
            $headers = [
                'Content-Type' => 'application/json',
                'Connection' => 'keep-alive'
            ];
            $url = $this->getEndpointUrl($scopeType, $scopeCode);
            $options = $this->getOptions($scopeType, $scopeCode);
            $username = $options['username'] ?? '';
            $password = $options['password'] ?? '';
            $curl->setHeaders($headers);
            $curl->setOption(CURLOPT_RETURNTRANSFER, true);
            $curl->setOption(CURLOPT_ENCODING, '');
            $curl->setOption(CURLOPT_CONNECTTIMEOUT, $options['connection_timeout']);
            $curl->setOption(CURLOPT_TIMEOUT, $options['execution_timeout']);
            $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
            $curl->setOption(CURLOPT_USERPWD, $username . ':' . $password);
            $curl->setOption(CURLOPT_FOLLOWLOCATION, true);
            $curl->setOption(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            try {
                $curl->post($url, $data);
                $body = $curl->getBody();
                $httpCode = $curl->getHttpCode();
                if ($this->isLoggerEnabled()) {
                    $this->logger->debug(print_r(
                        [
                            'url' => $url,
                            'data' => $data,
                            'httpCode' => $httpCode,
                            'response' => $body,
                        ],
                        true)
                    );
                }
                $result = $this->parseResponse($curl);
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                if (!$errorMessage) {
                    $errorMessage = __('An error has occurred while sending request');
                }
                $this->logger->error($errorMessage);
                $result = ['response' => '', 'errors' => $errorMessage];
            }
        }

        return $result;
    }

    /**
     * Is ERP API requests logger enabled
     *
     * @return bool
     */
    private function isLoggerEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(ErpConfigurationInterface::XML_PATH_LOGGER_ACTIVE);
    }

    /**
     * Parse curl response
     *
     * @param Curl $curl
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function parseResponse(ClientInterface $curl): array
    {
        $errors = null;
        $body = $curl->getBody();
        try {
            $response = $this->serializer->unserialize($body);
        } catch (InvalidArgumentException $e) {
            $response = $body;
        }
        $httpCode = $curl->getHttpCode();
        if ($httpCode >= 200 && $httpCode < 300) {
            $errors = null;
        } else {
            $errors = __('HTTP code ' . $httpCode .'. Bad response ' . $body);
        }

        return ['response' => $response, 'errors' => $errors];
    }

    /**
     * Get API endpoint URL
     *
     * @param string $scopeType
     * @param mixed|null $scopeCode
     * @return string|null
     */
    private function getEndpointUrl($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null): ?string
    {
        return $this->scopeConfig->getValue('beside_erp/' . $this->apiType .'/api_url', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return array
     */
    private function getOptions($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null): array
    {
        $options = [
            'attempts_count' => $this->erpConfiguration->getAttemptsCount(),
            'execution_timeout' => $this->erpConfiguration->getExecutionTimeout(),
            'connection_timeout' => $this->erpConfiguration->getConnectionTimeout(),
            'username' => $this->erpConfiguration->getAuthUsername($scopeType, $scopeCode),
            'password' => $this->erpConfiguration->getAuthPassword($scopeType, $scopeCode),
        ];

        return $options;
    }

    /**
     * Check if API is enabled
     *
     * @param string $apiType
     *
     * @return bool
     */
    private function isApiEnabled($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null): bool
    {
        return (bool) $this->scopeConfig->getValue('beside_erp/' . $this->apiType .'/active', $scopeType, $scopeCode);
    }

    /**
     * @param string $message
     * @param int $storeId
     * @return ErpRequestInterface|null
     * @throws Exception
     */
    public function saveToQueue(string $message, int $storeId): ?ErpRequestInterface
    {
        $result = null;
        if ($this->getApiType()) {
            $request = $this->erpRequestFactory->create();
            $request->setApiType($this->apiType)
                ->setStatus(ErpRequestInterface::STATUS_FAIL)
                ->setMessageToSend($message)
                ->setNumTries(0)
                ->setStoreId($storeId);

            try {
                $result = $this->erpRequestRepository->save($request);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        return $result;
    }
}
