<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Rma\Api\Data\RmaInterface;
use Magento\Rma\Api\Data\RmaSearchResultInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\HTTP\Client\CurlFactory;


/**
 * Class RmaDataFetcher
 *
 * @package Beside\Erp\Model
 */
class RmaDataFetcher
{
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var RmaRepositoryInterface
     */
    private RmaRepositoryInterface $rmaRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * @var Grid
     */
    private Grid $rmaGrid;

    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * RmaDataFetcher constructor.
     *
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RmaRepositoryInterface $rmaRepository
     * @param ProductRepositoryInterface $productRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param Grid $rmaGrid
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RmaRepositoryInterface $rmaRepository,
        ProductRepositoryInterface $productRepository,
        OrderRepositoryInterface $orderRepository,
        Grid $rmaGrid
    ) {

        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->rmaRepository = $rmaRepository;
        $this->productRepository = $productRepository;
        $this->rmaGrid = $rmaGrid;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Get RMA item details
     *
     * @param RmaInterface $rmaEntity
     * @param string $returnValue
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getDetails(RmaInterface $rmaEntity, string $returnValue): array
    {
        $data = [];
        $orderId = $rmaEntity->getOrderId();
        $rmaId = $rmaEntity->getEntityId();
        $order = $this->getOrder($orderId);
        $data['order_id'] = $orderId;
        $data['rma_id'] = $rmaId;
        $payment = $order->getPayment();
        if ($payment) {
            $paymentMethod = $this->getPaymentMethodCode($payment);
        }
        $data['payment_method'] = $paymentMethod ?? '';
        $rmaItems = $rmaEntity->getItems();
        foreach ($rmaItems as $item) {
            $line = [];
            $line['order_id'] = $orderId;
            $line['rma_id'] = $rmaId;
            $sku = $item->getProductSku();
            $line['sku'] = $sku;
            $line['id'] = (string) $item->getOrderItemId() ?? $this->getOrderItemIdBySku($order, $sku);
            $product = $this->productRepository->get($sku);
            $productId = $product->getId();
            $line['product_id'] = $productId;
            $line['qty_requested'] = $item->getQtyRequested() ? (string) $item->getQtyRequested() : null;
            $line['qty_authorized'] = $item->getQtyAuthorized() ? (string) $item->getQtyAuthorized() : null;
            $line['qty_approved'] = $item->getQtyApproved() ? (string) $item->getQtyApproved() : null;
            $line['qty_returned'] = $item->getQtyReturned() ? (string) $item->getQtyReturned() : null;
            $line['condition'] = $this->rmaGrid->getConditionOptionStringValue($item);
            $line['resolution'] = $this->rmaGrid->getResolutionOptionStringValue($item);
            $line['reason'] = $this->rmaGrid->getReasonOptionStringValue($item);
            $line['status'] = $item->getStatus();
            $line['return_value'] = $returnValue;
            $data['lines'][] = $line;
        }

        return $data;
    }

    /**
     * Get payment code from Order Payment
     *
     * @param OrderPaymentInterface $payment
     *
     * @return string
     */
    public function getPaymentMethodCode(OrderPaymentInterface $payment): string
    {
        $code = '';
        $paymentMethod = $payment->getMethod();
        switch ($paymentMethod) {
            case 'cashondelivery':
                $code = 'cod';
                break;
            case 'creditcard':
                $code = 'creditcard';
                break;
        }

        return $code;
    }

    /**
     * Prepare data for Create Return API from Order
     *
     * @param $orderId
     *
     * @return OrderInterface
     * @throws NoSuchEntityException
     */
    public function getOrder($orderId): OrderInterface
    {
        return $this->orderRepository->get($orderId);
    }

    /**
     * @param OrderInterface $order
     * @param string $sku
     * @return int|string|null
     */
    private function getOrderItemIdBySku(OrderInterface $order, string $sku)
    {
        $orderItemId = '';

        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($order->getItems() as $item) {
            if ($item->getSku() === $sku) {
                $orderItemId = $item->getItemId();
            }
        }

        return $orderItemId;
    }
}
