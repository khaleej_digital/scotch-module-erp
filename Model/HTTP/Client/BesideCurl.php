<?php
declare(strict_types=1);

namespace Beside\Erp\Model\HTTP\Client;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\ClientInterface;

/**
 * Class BesideCurl
 *
 * @package Beside\Erp\Model\HTTP\Client
 */
class BesideCurl extends Curl implements ClientInterface
{
    /**
     * HTTP code
     * @var int
     */
    private $httpCode;

    /**
     * Max supported protocol by curl CURL_SSLVERSION_TLSv1_2
     * @var int
     */
    private $sslVersion;

    /**
     * BesideCurl constructor.
     *
     * @param null $sslVersion
     */
    public function __construct($sslVersion = null)
    {
        parent::__construct($sslVersion);
    }

    /**
     * Make request
     * Extends original makeRequest method, adds setting http code from the response
     *
     * @param string $method
     * @param string $uri
     * @param array|string $params - use $params as a string in case of JSON or XML POST request.
     *
     * @return void
     */
    protected function makeRequest($method, $uri, $params = []): void
    {
        $this->_ch = curl_init();
        $this->curlOption(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS | CURLPROTO_FTP | CURLPROTO_FTPS);
        $this->curlOption(CURLOPT_URL, $uri);
        if ($method == 'POST') {
            $this->curlOption(CURLOPT_POST, 1);
            $this->curlOption(CURLOPT_POSTFIELDS, is_array($params) ? http_build_query($params) : $params);
        } elseif ($method == "GET") {
            $this->curlOption(CURLOPT_HTTPGET, 1);
        } else {
            $this->curlOption(CURLOPT_CUSTOMREQUEST, $method);
        }

        if (count($this->_headers)) {
            $heads = [];
            foreach ($this->_headers as $k => $v) {
                $heads[] = $k . ': ' . $v;
            }
            $this->curlOption(CURLOPT_HTTPHEADER, $heads);
        }

        if (count($this->_cookies)) {
            $cookies = [];
            foreach ($this->_cookies as $k => $v) {
                $cookies[] = "{$k}={$v}";
            }
            $this->curlOption(CURLOPT_COOKIE, implode(";", $cookies));
        }

        if ($this->_timeout) {
            $this->curlOption(CURLOPT_TIMEOUT, $this->_timeout);
        }

        if ($this->_port != 80) {
            $this->curlOption(CURLOPT_PORT, $this->_port);
        }

        $this->curlOption(CURLOPT_RETURNTRANSFER, 1);
        $this->curlOption(CURLOPT_HEADERFUNCTION, [$this, 'parseHeaders']);
        if ($this->sslVersion !== null) {
            $this->curlOption(CURLOPT_SSLVERSION, $this->sslVersion);
        }

        if (count($this->_curlUserOptions)) {
            foreach ($this->_curlUserOptions as $k => $v) {
                $this->curlOption($k, $v);
            }
        }

        $this->_headerCount = 0;
        $this->_responseHeaders = [];
        $this->_responseBody = curl_exec($this->_ch);
        $err = curl_errno($this->_ch);
        if ($err) {
            $this->doError(curl_error($this->_ch));
        }
        $this->setHttpCode();
        curl_close($this->_ch);
    }

    /**
     * Extract HTTP code and set it to the Curl object
     */
    private function setHttpCode(): void
    {
        $info = curl_getinfo($this->_ch);
        $this->httpCode = $info['http_code'] ?? 0;
    }

    /**
     * Get HTTP code
     *
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
