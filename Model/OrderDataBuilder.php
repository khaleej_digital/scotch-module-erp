<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Exception;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\GiftMessage\Api\OrderItemRepositoryInterface;
use PayTabs\PayPage\Model\Ui\ConfigProvider;
use Psr\Log\LoggerInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Fooman\Totals\Model\OrderTotalManagement;

/**
 * Class OrderDataBuilder
 *
 * @package Beside\Erp\Model
 */
class OrderDataBuilder
{
    /** @var string XML path to Paytabs email config setting */
    public const XML_PATH_PAYTABS_EMAIL_CONFIG = 'payment/creditcard/merchant_email';

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var OrderItemRepositoryInterface
     */
    private OrderItemRepositoryInterface $orderItemGiftRepository;

    /**
     * @var RuleRepositoryInterface
     */
    private RuleRepositoryInterface $ruleRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $orderCollection;

    /**
     * @var OrderTotalManagement
     */
    private OrderTotalManagement $orderTotalManagement;

    /**
     * @var ProductResource
     */
    private ProductResource $productResource;

    /**
     * OrderDataBuilder constructor.
     *
     * @param OrderItemRepositoryInterface $orderItemGiftRepository
     * @param RuleRepositoryInterface $ruleRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param LoggerInterface $logger
     * @param CollectionFactory $orderCollection
     * @param ScopeConfigInterface $scopeConfig
     * @param OrderTotalManagement $orderTotalManagement
     * @param ProductResource $productResource
     */
    public function __construct(
        OrderItemRepositoryInterface $orderItemGiftRepository,
        RuleRepositoryInterface $ruleRepository,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface $logger,
        CollectionFactory $orderCollection,
        ScopeConfigInterface $scopeConfig,
        OrderTotalManagement $orderTotalManagement,
        ProductResource $productResource
    ) {
        $this->orderItemGiftRepository = $orderItemGiftRepository;
        $this->scopeConfig = $scopeConfig;
        $this->ruleRepository = $ruleRepository;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->orderCollection = $orderCollection;
        $this->orderTotalManagement = $orderTotalManagement;
        $this->productResource = $productResource;
    }

    /**
     * Prepare data from order to API call
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    public function getOrderDataArray(OrderInterface $order): array
    {
        $customerId = $order->getCustomerId();
        $storeId = $order->getStoreId();
        $orderId = $order->getEntityId();
        $shippingAddress = $order->getShippingAddress();
        $shippingMethod = $order->getShippingMethod();
        $payment = $order->getPayment();
        $items = $this->getOrderLines($order);

        $data['order_id'] = (string) $orderId;
        $data['increment_id'] = (string) $order->getIncrementId();
        $data['source_id'] = $order->getBillingAddress()->getPickupLocationId();
        $data['language'] = $this->getLanguage($storeId);
        $data['customer_id'] = (string) $customerId;
        $data['customer_gender'] = (string) $this->resolveGender($order->getCustomerGender());
        $data['customer_prefix_name'] = $order->getCustomerPrefix();
        $data['customer_suffix_name'] = $order->getCustomerSuffix();
        $data['customer_first_name'] = $order->getCustomerFirstname();
        $data['customer_last_name'] = $order->getCustomerLastname();
        if ($shippingAddress) {
            $mobilePrefix = (string) $shippingAddress->getCustomerMobileNumberPrefix();
            $telephone = $shippingAddress->getTelephone();
        }
        $data['customer_mobile_number_prefix'] = $mobilePrefix ?? '';
        $data['customer_mobile_number'] = $telephone ?? '';
        $data['creation_date'] = $order->getCreatedAt();
        $data['delivery_date'] = $order->getDeliveryDate();
        $data['payment_method'] = $this->resolvePaymentMethodCode($payment->getMethod());
        $data['shipment_method'] = $this->resolveShippingMethodCode($shippingMethod);
        $data['carrier_code'] = $this->getCarrierCode($shippingMethod);
        $data['status'] = $order->getStatus();

        $data['addresses'] = $this->getAddresses($order);
        $data['invoice'] = $this->getInvoiceInfo($order);
        $data['payment'][] = $this->getPaymentData($order);

        $data['order_lines'] = $items['order_line'];

        if (isset($items['promotions_info'])) {
            $data['promotions_info'] = $items['promotions_info'];
        }

        return $data;
    }

    /**
     * @param string $paymentCode
     * @return string
     */
    private function resolvePaymentMethodCode($paymentCode)
    {
        switch ($paymentCode) {
            case 'cashondelivery':
                $paymentCode = 'cod';
                break;
            default:
                break;
        }

        return $paymentCode;
    }

    /**
     * TODO: enahce this method, to use values from DB.
     * @param string $gender
     * @return string
     */
    private function resolveGender($gender)
    {
        switch ($gender) {
            case '1':
                $gender = 'male';
                break;
            case '2':
                $gender = 'female';
                break;
            default:
                break;
        }

        return $gender;
    }

    /**
     * @param string $shippingtCode
     * @return string
     */
    private function resolveShippingMethodCode($shippingtCode)
    {
        return $shippingtCode === 'beside_store_pickup_beside_store_pickup' ? 'Store Pick Up' : 'Home Delivery';
    }

    /**
     * Returns carrier code "aramex" for home delivery and null for Click & Collect
     *
     * @param string $shippingMethod
     *
     * @return string|null
     */
    public function getCarrierCode(string $shippingMethod)
    {
        //TODO check if this needs to be changed
        return $this->scopeConfig->getValue(\Beside\Erp\Api\ErpConfigurationInterface::XML_PATH_CARRIER_CODE);
    }

    /**
     * Get order line items data
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    public function getOrderLines(OrderInterface $order): array
    {
        $orderItems = [];
        /** @var \Magento\Sales\Api\Data\OrderItemInterface[] $items */
        $items = $order->getAllVisibleItems();
        $orderId = $order->getEntityId();
        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($items as $item) {
            $orderLine = [];
            $promotions = [];
            $orderLine['order_id'] = (string) $orderId;
            $orderLine['id'] = (string) $item->getItemId();
            $orderLine['sku'] = (string) $item->getSku();
            $orderLine['product_id'] = (string) $this->productResource->getIdBySku($item->getSku());
            $orderLine['quantity'] = (string) $item->getQtyOrdered();
            $gross = (string) $item->getBaseRowTotalInclTax() ?? $item->getRowTotalInclTax();
            $orderLine['gross_amount'] = (string) $gross;
            $orderLine['tax_percent'] = (string) $item->getTaxPercent();
            $orderLine['gift_message'] = $this->getGiftMessage((int) $orderId, (int) $orderLine['id']);
            $orderLine['gift_wrapping'] = ((int)$order->getGwId()) ? 'yes' : 'no';

            $orderLine['net_amount'] = (string) $item->getBaseRowTotal();

            $ruleNames = '';
            $ruleIds = $item->getAppliedRuleIds();
            if ($ruleIds) {
                $ruleIds = explode(',', $ruleIds);
                $rules = [];
                try {
                    foreach ($ruleIds as $ruleId) {
                        $rule = $this->ruleRepository->getById($ruleId);
                        $ruleName = $rule->getName();
                        $rules[] = $ruleName;
                    }
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage());
                }
                $ruleNames = implode(', ', $rules);

                $promotions['order_id'] = (string) $orderId;
                $promotions['item_id'] = (string) $item->getItemId();
                $promotions['promotion_name'] = $ruleNames;
                $promotions['promotion_code'] = (string) $order->getCouponCode();
                $promotions['original_price'] = (string) $item->getOriginalPrice();
                $discount = $item->getBaseDiscountAmount();
                if ((int) $discount) {
                    $discountedPrice = (string) ($item->getBasePrice() - $item->getDiscountAmount());
                }
                $promotions['discounted_price'] = $discountedPrice ?? '0';
                $promotions['discount'] = (string) $item->getBaseDiscountAmount();

                $promotions['discount_percentage'] = (string) $item->getDiscountPercent();
            }
            $orderItems['order_line'][] = $orderLine;

            if (!empty($promotions)) {
                $orderItems['promotions_info'][] = $promotions;
            }
        }

        return $orderItems;
    }

    /**
     * Get order gift message
     *
     * @param int $orderId
     * @param int $orderItemId
     *
     * @return string
     */
    private function getGiftMessage(int $orderId, int $orderItemId): string
    {
        try {
            $giftMessage = $this->orderItemGiftRepository->get($orderId, $orderItemId);
            $giftMessage = $giftMessage->getMessage();
        } catch (NoSuchEntityException $e) {
            $giftMessage = '';
        }

        return $giftMessage;
    }

    /**
     * Get language code from store config
     *
     * @param mixed $storeId
     *
     * @return string
     */
    private function getLanguage($storeId): string
    {
        $language = $this->scopeConfig->getValue(
            'general/locale/code',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $language ?? '';
    }

    /**
     * Collect address info from order
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    private function getAddresses(OrderInterface $order): array
    {
        $orderAddresses[] = $order->getBillingAddress();
        $orderAddresses[] = $order->getShippingAddress();
        $orderId = $order->getEntityId();
        $addressesData = [];
        /** @var \Magento\Sales\Model\Order\Address $address */
        foreach ($orderAddresses as $address) {
            $addressData['order_id'] = (string) $orderId;
            $addressData['id'] = (string) $address->getEntityId();
            $addressData['address_type'] = $address->getAddressType();
            $street = $address->getStreet();
            $addressData['address1'] = $street[0] ?? '';
            $addressData['address2'] = $street[1] ?? '';
            $addressData['city'] = $address->getCity();
            $addressData['state'] = $address->getRegionCode();
            $addressData['zip'] = $address->getPostcode();
            $addressData['country_code'] = strtolower($address->getCountryId());
            $addressesData[] = $addressData;
        }

        return $addressesData;
    }

    /**
     * Collect invoice info
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    private function getInvoiceInfo(OrderInterface $order): array
    {
        $invoiceCollection = $order->getInvoiceCollection();
        $invoices = [];

        /** @var \Magento\Sales\Model\ResourceModel\Order\Invoice $invoice */
        foreach ($invoiceCollection as $invoice) {
            $invoiceId = $invoice->getEntityId();
            $invoiceData['id'] = (string) $invoiceId;
            $invoiceData['order_id'] = (string) $order->getEntityId();
            $invoiceData['gross_amount'] = (string) $order->getTotalInvoiced();
            $invoiceData['currency'] = $order->getOrderCurrencyCode();
            $invoices[] = $invoiceData;
        }

        return $invoices;
    }

    /**
     * Get Payment info
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    private function getPaymentData(OrderInterface $order): array
    {
        $payment = $order->getPayment();
        $paymentData['id'] = (string) $payment->getEntityId();
        $paymentData['order_id'] = (string) $order->getEntityId();
        $paymentData['currency'] = $order->getOrderCurrencyCode();
        $paymentData['gross_amount'] = (string) $payment->getAmountPaid();
        $paymentData['surcharge_amount'] = (string) $this->getSurchageAmount($order);

        $additionalInfo = $payment->getAdditionalInformation();
        // Those values (gateway_id and account) will comes from the payment gateway configuration.
        $paymentData['gateway_id'] = $additionalInfo['method_title'] ?? '';
        $paymentData['account'] = $this->getAccountName($payment);

        $paymentData['transaction_id'] = (string) $payment->getLastTransId();

        return $paymentData;
    }

    /**
     * Get Paytabs account name
     *
     * @param OrderPaymentInterface $payment
     *
     * @return string
     */
    private function getAccountName(OrderPaymentInterface $payment): string
    {
        $accountName = '';
        $method = $payment->getMethod();
        if ($method == ConfigProvider::CODE_CREDITCARD) {
            $accountName = (string) $this->scopeConfig->getValue(self::XML_PATH_PAYTABS_EMAIL_CONFIG);
        }

        return $accountName;
    }

    /**
     * Get Surchage Amount
     *
     * @param OrderInterface $order
     *
     * @return float:int
     */
    private function getSurchageAmount(OrderInterface $order)
    {
        $surcharge = 0;
        /** @var \Fooman\Totals\Api\Data\OrderTotalInterface[] $orderTotals */
        $orderTotals = $this->orderTotalManagement->getByOrderId(
            $order->getEntityId()
        );

        /** @var \Fooman\Totals\Api\Data\OrderTotalInterface $orderTotal */
        foreach ($orderTotals as $orderTotal) {
            $surcharge += $orderTotal->getAmount();
        }

        return $surcharge;
    }

    /**
     * Get unprocessed orders collection
     *
     * @return Collection
     */
    public function getErpUnprocessedOrders(): Collection
    {
        $collection = $this->orderCollection->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                ErpRequestInterface::ORDER_IS_PROCESSED_FLAG,
                0
            )->addFieldToFilter(
                ErpRequestInterface::NUM_TRIES,
                ['lt' => $this->scopeConfig->getValue(\Beside\Erp\Api\ErpConfigurationInterface::XML_PATH_ATTEMPTS_COUNT)]
            )->addFieldToFilter(
                'status',
                ['neq' => 'payment_review']
            )->join(
                ['sop' => $collection->getConnection()->getTableName('sales_order_payment')],
                'main_table.entity_id = sop.parent_id',
                'sop.method'
            );

        $whereExpression = '(main_table.grand_total <= IFNULL(main_table.total_paid, 0) AND sop.method = "creditcard") OR sop.method != "creditcard"';
        $collection->getSelect()
            ->where(new \Zend_Db_Expr($whereExpression));

        return $collection;
    }
}
