<?php
declare(strict_types=1);

namespace Beside\Erp\Model\ResourceModel;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class ErpRequest
 *
 * @package Beside\Erp\Model\ResourceModel
 */
class ErpRequest extends AbstractDb
{
    /**
     * @var string
     */
    const TABLE_NAME = 'beside_erp_queue';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ErpRequestInterface::ID);
    }
}
