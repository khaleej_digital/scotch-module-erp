<?php

namespace Beside\Erp\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class BulkProcessor
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * BulkProcessor constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param string $tableName
     * @param array $data
     */
    public function bulkUpdate(string $tableName, array $data)
    {
        if (!empty($data)) {
            $connection = $this->getConnection();
            $table = $connection->getTableName($tableName);

            $connection->insertOnDuplicate($table, $data);
        }
    }

    /**
     * @param string $tableName
     * @param array $data
     */
    public function bulkInsert(string $tableName, array $data)
    {
        if (!empty($data)) {
            $connection = $this->getConnection();
            $table = $connection->getTableName($tableName);

            $connection->insertMultiple($table, $data);
        }
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        if ($this->connection === null) {
            $this->connection = $this->resourceConnection->getConnection();
        }

        return $this->connection;
    }
}
