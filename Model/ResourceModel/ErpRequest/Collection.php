<?php
declare(strict_types=1);

namespace Beside\Erp\Model\ResourceModel\ErpRequest;

use Beside\Erp\Model\ErpRequest;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(ErpRequest::class,
            \Beside\Erp\Model\ResourceModel\ErpRequest::class);
    }
}
