<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\StockAggregateApiInterface;
use InvalidArgumentException;

/**
 * Class StockAggregateApi
 *
 * @package Beside\Erp\Model
 */
class StockAggregateApi extends ErpApi implements StockAggregateApiInterface
{
    /**
     * Api type
     * @var string
     */
    public $apiType = self::API_TYPE;

    /**
     * Prepare data, returns json-encoded string
     *
     * @param string $sku
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(string $sku): string
    {
        return $this->serializer->serialize(['sku' => $sku]);
    }
}
