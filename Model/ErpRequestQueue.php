<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\ErpApiInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Api\Data\ErpRequestInterface;
use Exception;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;

/**
 * Class ErpRequestQueue
 *
 * @package Beside\Erp\Model
 */
class ErpRequestQueue
{
    /**
     * @var ErpRequestRepositoryInterface
     */
    private ErpRequestRepositoryInterface $erpRequestRepository;

    /**
     * @var ErpRequestInterface
     */
    private ErpRequestInterface $erpRequest;

    /**
     * @var Json
     */
    private Json $json;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * ERP API array
     * @var array
     */
    private array $erpApiArray;

    /**
     * ErpRequestQueue constructor.
     *
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     * @param Json $json
     * @param LoggerInterface $logger
     * @param ErpRequestInterface $erpRequest
     * @param array $erpApiArray
     */
    public function __construct(
        ErpRequestRepositoryInterface $erpRequestRepository,
        Json $json,
        LoggerInterface $logger,
        ErpRequestInterface $erpRequest,
        array $erpApiArray = []
    ) {
        $this->erpRequestRepository = $erpRequestRepository;
        $this->erpRequest = $erpRequest;
        $this->json = $json;
        $this->logger = $logger;
        $this->erpApiArray = $erpApiArray;
    }

    /**
     * Update request of queue
     *
     * @param ErpRequest $request
     */
    public function update(ErpRequest &$request) : void
    {
        $storeCode = $request->getStoreId();
        $scopeType = $storeCode === null
            ? \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT
            : \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

        try {
            /** @var ErpApiInterface $api */
            $api = $this->erpApiArray[$request->getApiType()];
            $response = $api->sendRequest($request->getMessageToSend(), $scopeType, $storeCode);

            $request->setNumTries(1 + $request->getNumTries());
            if (!empty($response['errors'])) {
                $status = ErpRequestInterface::STATUS_FAIL;
            } else {
                $status = ErpRequestInterface::STATUS_SUCCESS;
            }
            $request->setStatus($status);
            $request->setApiResponse($this->json->serialize($response));
            $this->erpRequestRepository->save($request);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
