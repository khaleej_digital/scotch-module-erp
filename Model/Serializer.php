<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use InvalidArgumentException;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Serializer
 *
 * @package Beside\Erp\Model
 */
class Serializer implements SerializerInterface
{
    /** @var SerializerInterface  */
    private SerializerInterface $serializer;

    /**
     * Serializer constructor.
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Returns json string.
     * Use default JSON_PRESERVE_ZERO_FRACTION to encode float values as float.
     *
     * @param string|int|float|bool|array|null $data
     * @return string|bool
     *
     * @throws InvalidArgumentException
     */
    public function serialize($data)
    {
        $result = json_encode($data, JSON_PRESERVE_ZERO_FRACTION);
        if (false === $result) {
            throw new InvalidArgumentException("Unable to serialize value. Error: " . json_last_error_msg());
        }
        return $result;
    }

    /**
     * Unserialize the given string
     *
     * @param string $string
     *
     * @return string|int|float|bool|array|null
     * @throws InvalidArgumentException
     */
    public function unserialize($string)
    {
        return $this->serializer->unserialize($string);
    }
}
