<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Beside\Erp\Api\Data\ErpRequestSearchResultInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Beside\Erp\Model\ResourceModel\ErpRequest\Collection;
use Beside\Erp\Model\ResourceModel\ErpRequest\CollectionFactory;
use Beside\Erp\Api\Data\ErpRequestSearchResultInterfaceFactory;
use Beside\Erp\Model\ResourceModel\ErpRequest as ErpRequestResource;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;


/**
 * Class ErpRequestRepository
 *
 * @package Beside\Erp\Model
 */
class ErpRequestRepository implements ErpRequestRepositoryInterface
{
    /**
     * @var array
     */
    private array $registry = [];

    /**
     * @var ErpRequestFactory
     */
    private ErpRequestFactory $requestFactory;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var ErpRequestSearchResultInterfaceFactory
     */
    private ErpRequestSearchResultInterfaceFactory $searchResultsFactory;

    /**
     * @var ErpRequestResource
     */
    private ErpRequestResource $requestResource;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * ErpRequestRepository constructor.
     *
     * @param ErpRequestResource $erpRequestResource
     * @param ErpRequestFactory $requestFactory
     * @param CollectionFactory $collectionFactory
     * @param ErpRequestSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ErpRequestResource $erpRequestResource,
        ErpRequestFactory $requestFactory,
        CollectionFactory $collectionFactory,
        ErpRequestSearchResultInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->requestResource = $erpRequestResource;
        $this->requestFactory = $requestFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Get info about request by request id
     *
     * @param int $id
     * @return ErpRequestInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): ErpRequestInterface
    {
        if (!array_key_exists($id, $this->registry)) {
            $request = $this->requestFactory->create();
            $this->requestResource->load($request, $id);
            if (!$request->getId()) {
                throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
            }
            $this->registry[$id] = $request;
        }

        return $this->registry[$id];
    }

    /**
     * Get list of request
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ErpRequestSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        $collection = $collection->load();

        /** @var ErpRequestSearchResultInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    /**
     * Save request
     *
     * @param ErpRequestInterface $request
     * @return ErpRequestInterface
     * @throws CouldNotSaveException
     */
    public function save(ErpRequestInterface $request): ErpRequestInterface
    {
        /** @var ErpRequestResource $request */
        $this->requestResource->save($request);

        return $request;
    }

    /**
     * Delete request
     *
     * @param ErpRequestInterface $request
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ErpRequestInterface $request): bool
    {
        try {
            /** @var ErpRequestResource $request */
            $this->requestResource->delete($request);
            unset($this->registry[$request->getId()]);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove request #%1', $request->getId()));
        }

        return true;
    }

    /**
     * Delete request by Id
     *
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }
}
