<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\GetOrderDetailsApiInterface;
use InvalidArgumentException;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class GetOrderDetailsApi
 *
 * @package Beside\Erp\Model
 */
class GetOrderDetailsApi extends ErpApi implements GetOrderDetailsApiInterface
{
    /**
     * Api type
     *
     * @var string
     */
    public $apiType = self::API_TYPE;

    /**
     * Prepare data from order to API call
     *
     * @param OrderInterface $order
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(OrderInterface $order): string
    {
        $orderData = ['order_id' => $order->getEntityId()];
        $message = $this->serializer->serialize($orderData);

        return $message;
    }
}
