<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\StockCheckApiInterface;
use InvalidArgumentException;

/**
 * Class StockCheckApi
 *
 * @package Beside\Erp\Model
 */
class StockCheckApi extends ErpApi implements StockCheckApiInterface
{
    /**
     * Api type
     * @var string
     */
    public $apiType = self::API_TYPE;

    /**
     * Prepare data, return json-encoded string
     *
     * @param string $sku
     * @param string|null $sourceId
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function prepareData(string $sku, $sourceId = null): string
    {
        return $this->serializer->serialize(['sku' => $sku, 'source_id' => $sourceId]);
    }
}
