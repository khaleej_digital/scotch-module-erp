<?php
declare(strict_types=1);

namespace Beside\Erp\Model;

use Beside\Erp\Api\CreateTransferOrderApiInterface;

/**
 * Class CreateTransferOrderApi
 *
 * @package Beside\Erp\Model
 */
class CreateTransferOrderApi extends ErpApi implements CreateTransferOrderApiInterface
{
    /**
     * Api type
     * @var string
     */
    public $apiType = self::API_TYPE;
}
